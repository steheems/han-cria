# README #

##Through The Mud##
A rich internet application for creating your own car, for use in an externally developed game about rally racing.

### What is this repository for? ###

This code was created for the semester CRIA (Create a Rich Internet Application) at the Hogeschool van Arnhem en Nijmegen, university of applied sciences.

### Requirements ###

* Node.js
* MongoDB

### Set up ###

* Clone this repo
* Start MongoDB
* Open a command prompt / terminal in the `/server` folder
* Run `npm install`
* After it is done, run `node app.js` to start the app
* Go to `localhost:30000` to use the app

### Directory structure ###

* client | Folder with the client side of the application
    * app | Folder with the Angular application
        * customize | Folder with controllers and partials for the customize page
            * controllers
                * CustomizeController.js
            * partials
                * customize.html
        * directives | Folder with Angular directives, small pieces of the website which are reusable
            * headerDirective.html
            * headerDirective.js
            * hoverDirective.js
            * loginDirective.html
            * loginDirective.js
        * home | Folder with controllers and partials for the home page
            * controllers
                * HomeController.js
            * partials
                * home.html
        * app.js
        * BackgroundController.js
        * route.js
        * services.js
    * css | Folder for the stylesheets of the web application
    * image | Folder for the images of the web application
    * script | Folder for JavaScript other than the Angular application
    * index.html
* server | Folder with the server side of the application
    * app | Folder with controllers and models for the Express app
        * controllers | Folder with controllers
            * accountController.js
            * classController.js
            * colorController.js
            * patternController.js
            * rimController.js
            * schoolController.js
        * models | Folder with MongoDB models
            * account.js
            * class.js
            * color.js
            * pattern.js
            * rim.js
            * school.js
    * config | Folder with configuration files
        * config.js
        * localStrategy.js | Passport strategy for authentication
    * routes | http routes for the REST api / Express server
        * accountRoute.js
        * authRoute.js
        * classRoute.js
        * colorRoute.js
        * patternRoute.js
        * rimRoute.js
        * schoolRoute.js
    * uploads | Folder in which the uploads of users will be put
        * Pattern
        * Sticker
    * .gitignore
    * app.js
    * package.json
    * passport.js

### Creators ###

CRIA project group 8
Period: Sep2014

* Erwin Heemsbergen (front- & back-end programmer)
* Tamara van Wijk (designer and front-end programmer)
* Hai Feng Lin (front-end programmer)

* Nikai de Gans (designer)
* Ruben Ririmasse (designer)