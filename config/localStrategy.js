/*jslint nomen: true */
/*global module:true, require: true */
/**
 * Module dependencies.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    Account = require('mongoose').model('Account');

module.exports = function () {
    'use strict';
    // Use local strategy
    passport.use(new LocalStrategy({
        usernameField: '_id',
        passwordField: 'password'
    },
        function (username, password, done) {
            Account.findOne({
                _id: username
            }, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }

                return done(null, user);
            });
        }));
};
