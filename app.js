/*jslint browser: true, plusplus: true, nomen: true */
/*global require: true, process: true, console: true, __dirname: true */

/**
 * Created by Erwin Heemsbergen
 *
 * Based on 'MEAN with books' by Theo Theunissen
 */

/**
 * Module dependencies.
 * @type {exports}
 */
var express = require('express'),
    fs = require('fs'),
    http = require('http'),
    path = require('path');

/**
 * Load configuration
 * @type {*|string}
 */
var env = process.env.NODE_ENV || 'development',
    config = require('./config/config.js')[env];

global.myDirName = __dirname;

if (process.argv[2] != undefined) {
    myDirName = process.argv[2];
}

/**
 * Load Mongoose
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

mongoose.connect(config.db);

/**
 * Debugging
 */
mongoose.connection.on('error', function (err) {
    'use strict';
    console.error('MongoDB error: %s', err);
});

mongoose.set('debug', config.debug);

/**
 * Express middleware
 */
var morgan = require('morgan'),                     //Used to be express.logger
    bodyParser = require('body-parser'),            //Used to be bodyParser, json and urlencoded
    methodOverride = require('method-override'),    //Used to be express.methodOverride
    expressSession = require('express-session'),    //Used to be express.session
    serveStatic = require('serve-static'),          //Used to be express.static
    cookies = require('cookies'),
    keygrip = require('keygrip'),
    helmet = require('helmet'),
    passport = require('passport'),
    errorHandler = require('errorhandler'),
    multer = require('multer');

/**
 * Bootstrap models
 * @type {string}
 */
var models_path = myDirName + '/app/models';

require(models_path + '/account.js');
require(models_path + '/class.js');
require(models_path + '/color.js');
require(models_path + '/pattern.js');
require(models_path + '/rim.js');
require(models_path + '/school.js');
require(models_path + '/upgrade.js');
require(models_path + '/highscore.js');

/**
 * Use express
 * @type {*}
 */
var app = express();

/**
 * Configure express
 */
app.set('port', process.env.PORT || config.port);               // the port number where the app will listen to
app.use(morgan('dev'));                                         // values: default, short, tiny, dev
app.use(bodyParser.json({limit: '50mb'}));                                     // For security sake, it's better to disable file upload if your application doesn't need it. To do this, use only the needed middleware, i.e. don't use the bodyParser and multipart() middleware:
app.use(bodyParser.urlencoded({extended: true}));               // accepting urlencoded requests
app.use(multer({dest: './uploads'}));                           // accepting file upload
app.use(methodOverride());                                      // used for REST to simulate PUT and DELETE
// var keys = keygrip(['your secret here']);
// app.use(cookies.express(keys));                                 // cookieParser parses the contents of the Cookie header (utilizing the aptly-named cookie-module) and conveniently places the result (an object keyed by the cookie names) in req.cookies for your enjoyment.
app.use(expressSession({                                        // Using sessions to keep track of users as they journey through your site is key to any respectable application. Luckily, as usual, using Express with your Node.js application makes it super simple to get sessions up and running.
    secret: 'Th1s 1s a Sup3r 53cr37 S3cr3t',
    resave: true,
    saveUninitialized: true
}));

// use passport session
app.use(passport.initialize());
app.use(passport.session());

require('./passport')();

// Use helmet to secure Express headers
app.use(helmet.xframe());
app.use(helmet.xssFilter());
app.use(helmet.nosniff());
app.use(helmet.ienoopen());
app.disable('x-powered-by');

//app.use(serveStatic(path.join(dirName, '../client')));     // path for serving static html

/**
 * Errorhandler
 */
if (env === 'development') {
    app.use(errorHandler());
}

/**
 * Bootstrap http server
 */
http.createServer(app).listen(app.get('port'), function () {
    'use strict';
    console.log("Express server listening on port " + app.get('port'));
});

/**
 * Bootstrap routes
 * @type {string}
 */
var routes_path = myDirName + '/routes',
    route_files = fs.readdirSync(routes_path);

route_files.forEach(function (file) {
    'use strict';
    require(routes_path + '/' + file)(app);
});

app.use(serveStatic(myDirName, '/uploads'));
app.use(serveStatic(path.join(myDirName, '/client')));     // path for serving static html
