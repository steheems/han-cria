/*global module:true, require: true */
var isAuthenticated = function (req, res, next) {
    'use strict';
    if (req.isAuthenticated()) {
        return next();
    }
    res.sendStatus(401);
};

module.exports = function (app) {
    'use strict';
    var collectionName = 'patterns',
        controller = require('../app/controllers/patternController.js');

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieveAll);

    // RETRIEVE PERSONAL
    app.get('/' + collectionName + '/:id', isAuthenticated, controller.retrievePersonal);
    
    // ADD
    app.post('/' + collectionName + '/:id', isAuthenticated, controller.add);

};