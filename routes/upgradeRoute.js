/**
 * Created by Ernie on 27-1-2015.
 */
/*global module:true, require: true */
var isAuthenticated = function (req, res, next) {
    'use strict';
    if (req.isAuthenticated()) {
        if(req.user._id == req.params.id) {
            return next();
        }
    }
    res.sendStatus(401);
};

module.exports = function (app) {
    'use strict';
    var collectionName = 'upgrades',
        controller = require('../app/controllers/upgradeController.js');

    // CREATE
    //app.post('/' + collectionName, controller.create);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', isAuthenticated, controller.retrieve);

    // CREATE OR UPDATE
    app.post('/' + collectionName + '/:id', isAuthenticated, controller.createOrUpdate);
};