/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var collectionName = 'schools',
        controller = require('../app/controllers/schoolController.js'),
        classController = require('../app/controllers/classController.js');

    // CREATE
    app.post('/' + collectionName, controller.create);

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieve);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', controller.retrieve);
    
    // RETRIEVE CLASSES FROM A SCHOOL
    app.get('/' + collectionName + '/:id/classes', classController.retrieveBySchool);

    // UPDATE
    app.post('/' + collectionName + '/:id', controller.update);

    // DELETE
    app.delete('/' + collectionName + '/:id', controller.delete);
};