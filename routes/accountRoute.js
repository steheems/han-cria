/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var collectionName = 'accounts',
        controller = require('../app/controllers/accountController.js');

    // CREATE
    app.post('/' + collectionName, controller.create);

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieve);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', controller.retrieve);

    // UPDATE
    app.post('/' + collectionName + '/:id', controller.update);
    app.put('/' + collectionName + '/:id', controller.update);

    // UPDATE LEVEL UNLOCKED
    app.post('/' + collectionName + '/:id/level/:lvl', controller.updateLevelUnlocked);
    app.put('/' + collectionName + '/:id/level/:lvl', controller.updateLevelUnlocked);

    // UPDATE DESIGN
    app.post('/' + collectionName + '/:id/design', controller.updateDesign);
    app.put('/' + collectionName + '/:id/design', controller.updateDesign);

    // DELETE
    app.delete('/' + collectionName + '/:id', controller.delete);
};