/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var collectionName = 'classes',
        controller = require('../app/controllers/classController.js');

    // CREATE
    app.post('/' + collectionName, controller.create);

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieve);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', controller.retrieve);

    // UPDATE
    app.post('/' + collectionName + '/:id', controller.update);

    // DELETE
    app.delete('/' + collectionName + '/:id', controller.delete);
};