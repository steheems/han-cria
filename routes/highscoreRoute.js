/**
 * Created by erwin on 03-02-15.
 */
module.exports = function (app) {
    'use strict';
    var collectionName = 'highscores',
        controller = require('../app/controllers/highscoreController.js');

    // CREATE
    app.post('/' + collectionName + '/:level', controller.create);

    // RETRIEVE ALL
    app.get('/' + collectionName + '/:level', controller.retrieve);

    // RETRIEVE ALL BY SCHOOL
    app.get('/' + collectionName + '/:level/by-school/:id', controller.retrieveBySchool);

    // RETRIEVE ALL BY CLASS
    app.get('/' + collectionName + '/:level/by-class/:id', controller.retrieveByClass);

    // RETRIEVE ALL BY ACCOUNT
    app.get('/' + collectionName + '/:level/by-account/:id', controller.retrieveByAccount);
};