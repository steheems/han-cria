/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var collectionName = 'rims',
        controller = require('../app/controllers/rimController.js');

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieve);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', controller.retrieve);

};