/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var passport = require('passport');

    // route to know whether or not somone is logged in
    app.get('/login', function (req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    });

    // route to log in
    app.post('/login', passport.authenticate('local'), function (req, res) {
        req.user.salt = '';
        req.user.password = '';
        res.send(req.user);
    });
    
    // route to log out
    app.get('/logout', function (req, res) {
        req.logOut();
        res.sendStatus(200);
    });

    // route to log out
    app.post('/logout', function (req, res) {
        req.logOut();
        res.sendStatus(200);
    });
};