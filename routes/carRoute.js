/*global module:true, require: true */
var isAuthenticated = function (req, res, next) {
    'use strict';
    if (req.isAuthenticated()) {
        return next();
    }
    res.sendStatus(401);
};

module.exports = function (app) {
    'use strict';
    var collectionName = 'cars',
        controller = require('../app/controllers/carController.js');

    // ADD
    app.post('/' + collectionName + '/:id/:view', isAuthenticated, controller.add);
};