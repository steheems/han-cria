/*global module:true, require: true */
module.exports = function (app) {
    'use strict';
    var collectionName = 'colors',
        controller = require('../app/controllers/colorController.js');

    // RETRIEVE ALL
    app.get('/' + collectionName, controller.retrieve);

    // RETRIEVE ONE
    app.get('/' + collectionName + '/:id', controller.retrieve);

};