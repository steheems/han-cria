/*global ttm: true */
ttm.config(['$routeProvider', function ($routeProvider) {
    'use strict';

    // Home

    $routeProvider.when('/', {
        templateUrl: 'app/home/partials/home.html',
        controller: 'HomeController'
    });

    // Register

    $routeProvider.when('/registreren', {
        templateUrl: 'app/home/partials/register.html',
        controller: 'RegisterController'
    });

    // Customize

    $routeProvider.when('/customize', {
        templateUrl: 'app/customize/partials/customize.html',
        controller: 'CustomizeController'
    });

    // My profile

    $routeProvider.when('/mijnProfiel', {
        templateUrl: 'app/home/partials/myProfile.html',
        controller: 'MyProfileController'
    });
}]);