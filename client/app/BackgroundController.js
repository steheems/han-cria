/*global ttm: true */
/**
 * Created by Erwin Heemsbergen on 7-1-2015.
 */
ttm.controller('BackgroundController', ['$scope', '$location', function ($scope, $location) {
    'use strict';
    $scope.$on('$routeChangeSuccess', function () {
        var bodyStyle;
        if ($location.path() === '/customize') {
            bodyStyle = {"background-image": "url('../image/Home/Background.jpg')"};
        } else {
            bodyStyle = {"background-image": "url('../image/Home/Background.jpg')"};
        }

        $scope.bodyStyle = bodyStyle;
    });
}]);