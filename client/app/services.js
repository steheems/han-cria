/*jslint nomen: true */
/*global ttm: true */
ttm.factory('TestsService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.tests = $resource('/tests/:id/:perform', {}, actions);
        return db;
    }]);

ttm.factory('RequestsService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.requests = $resource('/requests/:id/:perform', {}, actions);
        return db;
    }]);

ttm.factory('AccountsService', ['$http', '$resource',
    function ($http, $resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.accounts = $resource('/accounts/:id', {}, actions);

        db.updateDesign = function (id, designSide, designTop) {
            return $http.post('/accounts/' + id + '/design', {designSide: designSide, designTop: designTop}).then(function (res) {
                return res;
            });
        };

        return db;
    }]);

ttm.factory('AuthService', ['$http', 'AccountsService',
    function ($http, AccountsService) {
        'use strict';
        var authService = {};

        authService.login = function (user) {
            return $http.post('/login', {_id: user.id, password: user.password}).then(function (res) {
                return res;
            });
        };

        authService.loginTeacher = function (teacher) {
            return AccountsService.accounts.get({email: teacher.email}).$promise.then(function (data) {
                if(data.doc.length === 0) {
                    throw new Error('Account met dit e-mailadres kon niet worden gevonden');
                }

                return $http.post('/login', {_id: data.doc[0]._id, password: teacher.password}).then(function (res) {
                    return res;
                });
            });
        };

        authService.logout = function () {
            return $http.get('/logout');
        };

        authService.loggedin = function () {
            return $http.get('/login').then(function (res) {
                return res;
            });
        };

        return authService;
    }]);

ttm.factory('PatternsService', ['$http',
    function ($http) {
        'use strict';
        var patternsService = {};

        patternsService.getPatterns = function () {
            return $http.get('/patterns').then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        patternsService.getPersonalPatterns = function (id) {
            return $http.get('/patterns/' + id).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        return patternsService;
    }]);

ttm.factory('ClassesService', ['$resource',
    function ($resource) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.classes = $resource('/classes/:id', {}, actions);
        return db;
    }]);

ttm.factory('SchoolsService', ['$resource', '$http',
    function ($resource, $http) {
        'use strict';

        var actions = {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'update': {method: 'PUT'},
            'query': {method: 'GET', isArray: true},
            'delete': {method: 'DELETE'}
        }, db = {};

        db.schools = $resource('/schools/:id', {}, actions);

        db.classesBySchool = function (schoolId) {
            return $http.get('/schools/' + schoolId + '/classes').then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        return db;
    }]);

ttm.factory('CarsService', ['$http',
    function ($http) {
        'use strict';
        var carsService = {};

        carsService.uploadDesign = function (userId, imageData, view) {
            return $http.post('/cars/' + userId + '/' + view, {data: imageData}).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        return carsService;
    }]);

ttm.factory('HighscoresService', ['$http',
    function ($http) {
        'use strict';
        var highscoresService = {};

        highscoresService.getAllScores = function (level) {
            return $http.get('/highscores/' + level).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        highscoresService.getBySchool = function (level, schoolId) {
            return $http.get('/highscores/' + level + '/by-school/' + schoolId).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        highscoresService.getByClass = function (level, classId) {
            return $http.get('/highscores/' + level + '/by-class/' + classId).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        highscoresService.getByAccount = function (level, accountId) {
            return $http.get('/highscores/' + level + '/by-account/' + accountId).then(function (res) {
                if (res.data.err) {
                    throw res.data.err;
                }

                return res;
            });
        };

        return highscoresService;
    }]);