/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 7-1-2015.
 */
ttm.controller('HomeController', ['$rootScope', '$scope', 'HighscoresService', 'AuthService', 'AccountsService', 'ClassesService', function ($rootScope, $scope, HighscoresService, AuthService, AccountsService, ClassesService) {
    'use strict';

    $scope.level = 0;
    $scope.typeFilter = 'world';

    $scope.loggedIn = false;

    AuthService.loggedin().then(function (data) {
        $scope.currentUser = data.data;
        if ($scope.currentUser !== '0') {
            $scope.loggedIn = true;
        }
    });

    $rootScope.$on('loggedIn', function() {
        $scope.loggedIn = true;
    });

    $rootScope.$on('loggedOut', function() {
        $scope.loggedIn = false;
        $scope.loadHighscores(1, 'all');
        $scope.filterHighScores('world');
    });

    $scope.makeScrollable = function () {
        $('body').css('overflow', 'auto');
    };

    $scope.filterHighScores = function (typeFilter) {
        var idWorld, idSchool, idClass, idIndividual;

        idWorld = $("#filterWorld");
        idSchool = $("#filterSchool");
        idClass = $("#filterClass");
        idIndividual = $("#filterIndividual");

        $scope.typeFilter = typeFilter;

        if (typeFilter === 'world') {
            idWorld.addClass("border");
            idSchool.removeClass("border");
            idClass.removeClass("border");
            idIndividual.removeClass("border");
        } else if (typeFilter === 'school') {
            idWorld.removeClass("border");
            idSchool.addClass("border");
            idClass.removeClass("border");
            idIndividual.removeClass("border");
        } else if (typeFilter === 'class') {
            idWorld.removeClass("border");
            idSchool.removeClass("border");
            idClass.addClass("border");
            idIndividual.removeClass("border");
        } else {
            idWorld.removeClass("border");
            idSchool.removeClass("border");
            idClass.removeClass("border");
            idIndividual.addClass("border");
        }
    };

    $scope.loadHighscores = function (level, category) {
        AuthService.loggedin().then(function (data) {
            $scope.currentUser = data.data;

            if (category === null || category === 'world') {
                HighscoresService.getAllScores(level).then(function (data) {
                    $scope.highscores = data.data.doc;
                    $scope.getHighscoreAccountNames();
                    $scope.clusterHighscores();
                    $scope.viewCar($scope.highscores[0]);
                })
            } else {
                if ($scope.currentUser !== '0') {
                    if (category === 'class') {
                        HighscoresService.getByClass(level, $scope.currentUser.class).then(function (data) {
                            $scope.highscores = data.data.doc;
                            $scope.getHighscoreAccountNames();
                            $scope.clusterHighscores();
                            $scope.viewCar($scope.highscores[0]);
                        });
                    } else if (category === 'school') {
                        ClassesService.classes.get({id: $scope.currentUser.class}).$promise.then(function (data) {
                            HighscoresService.getBySchool(level, data.doc.school._id).then(function (data) {
                                $scope.highscores = data.data.doc;
                                $scope.getHighscoreAccountNames();
                                $scope.clusterHighscores();
                                $scope.viewCar($scope.highscores[0]);
                            });
                        });
                    } else if (category === 'account') {
                        HighscoresService.getByAccount(level, $scope.currentUser._id).then(function (data) {
                            $scope.highscores = data.data.doc;
                            $scope.getHighscoreAccountNames();
                            $scope.clusterHighscores();
                            $scope.viewCar($scope.highscores[0]);
                        });
                    }
                } else {
                    throw new Error('User not logged in');
                }
            }
        });
    };

    $scope.loadHighscores($scope.level, $scope.typeFilter);

    $scope.changeLevel = function (direction) {
        if (direction === 'down' && $scope.level > 0) {
            $scope.level--;
            $scope.loadHighscores($scope.level, $scope.typeFilter);
        } else if (direction === 'up' && $scope.level < 10) {
            $scope.level++;
            $scope.loadHighscores($scope.level, $scope.typeFilter);
        }
    };

    $scope.getFullName = function (account) {
        if (account !== undefined) {
            if (account.firstName !== undefined && account.lastName !== undefined) {
                var fullName = '';
                fullName += account.firstName;

                if (account.preposition !== undefined && account.preposition !== '') {
                    fullName += ' ' + account.preposition;
                }

                fullName += ' ' + account.lastName;

                return fullName;
            }
        }

        return null;
    };

    $scope.getHighscoreAccountNames = function () {
        $.each($scope.highscores, function(scoreIndex, score) {
            if (score.account !== undefined) {
                AccountsService.accounts.get({id: score.account}).$promise.then(function (data) {
                    score.name = $scope.getFullName(data.doc);
                });
            }
        });
    };

    $scope.clusterHighscores = function () {
        $scope.highscoresClustered = [];

        var noOfSubArrays = Math.floor($scope.highscores.length / 5);
        var rest = $scope.highscores.length % 5;
        var totalCounter = 0;

        if (noOfSubArrays > 0) {
            for (var i = 0; i < noOfSubArrays; i++) {
                $scope.highscoresClustered[i] = [];
                for (var j = 0; j < 5; j++) {
                    $scope.highscoresClustered[i][j] = $scope.highscores[totalCounter];
                    $scope.highscoresClustered[i][j].ranking = totalCounter + 1;
                    totalCounter++;
                }
            }
        }

        if (rest > 0) {
            $scope.highscoresClustered[noOfSubArrays] = [];
            for (var j = 0; j < rest; j++) {
                $scope.highscoresClustered[noOfSubArrays][j] = $scope.highscores[totalCounter];
                $scope.highscoresClustered[noOfSubArrays][j].ranking = totalCounter + 1;
                totalCounter++;
            }
        }
    };

    $scope.viewCar = function (highscore) {
        var idCar, idLi, img = new Image(), imgSrc;

        imgSrc = "/uploads/cars/" + highscore.account + '/side.png';
        img.src = imgSrc;

        img.onload = function () {
            $(".highscore-item").removeClass("selectedHighscore");

            idCar = document.getElementById("currentHighScoreCar");

            if (img.height > 0) {
                idCar.src = imgSrc;
            } else {
                idCar.src = 'image/Auto/AutoZij.png';
            }

            idLi = $("#highscore-" + highscore.ranking);

            idLi.addClass("selectedHighscore");
        };

        img.onerror = function () {
            $(".highscore-item").removeClass("selectedHighscore");

            idCar = document.getElementById("currentHighScoreCar");

            idCar.src = 'image/Auto/AutoZij.png';

            idLi = $("#highscore-" + highscore.ranking);

            idLi.addClass("selectedHighscore");
        };
    };

    $scope.changeScoresPage = function (page) {
        $('.listHighscores').hide();

        $('#list-highscores-' + page).show();

        $scope.pagerSelector(page);
    };

    $scope.pagerSelector = function (page) {
        $('.activePager').addClass('nonActivePager').removeClass('activePager');

        $('#pager' + page).addClass('activePager').removeClass('nonActivePager');
    };
}]);