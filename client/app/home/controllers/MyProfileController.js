/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 7-1-2015.
 */
ttm.controller('MyProfileController', ['$rootScope', '$scope', '$location', 'AuthService', 'AccountsService', 'SchoolsService', 'ClassesService', function ($rootScope, $scope, AuthService, AccountsService, SchoolsService, ClassesService) {
    'use strict';

    AuthService.loggedin().then(function (data) {
        $scope.currentUser = data.data;

        if ($scope.currentUser !== '0') {
            console.log(data);
            ClassesService.classes.get({id:$scope.currentUser.class}).$promise.then(function (hulp) {
                $scope.class = hulp.doc;
            });
        }
    });

    $rootScope.$on('loggedOut', function() {
        // Redirects to homepage
        $location.url("/");
    });


    $scope.getFullName = function (account) {
        if (account !== undefined) {
            if (account.firstName !== undefined && account.lastName !== undefined) {
                var fullName = '';
                fullName += account.firstName;

                if (account.preposition !== undefined && account.preposition !== '') {
                    fullName += ' ' + account.preposition;
                }

                fullName += ' ' + account.lastName;

                return fullName;
            }
        }

        return null;
    };

    $scope.getEmail = function (account) {
        if (account !== undefined) {
            if (account.isTeacher === true) {
                $("#myProfileEmail").show();
                $("#myProfileCustomizeStudents").show();
                return account.email;
            } else {
                $("#myProfileEmail").hide();
                $("#myProfileCustomizeStudents").hide();
            }
        }
    }
}]);