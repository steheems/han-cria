/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 7-1-2015.
 */
ttm.controller('RegisterController', ['$scope', '$location', 'SchoolsService', 'AccountsService', function ($scope, $location, SchoolsService, AccountsService) {
    'use strict';

    $scope.disabled = true;

    SchoolsService.schools.get().$promise.then(function (data) {
        $scope.schools = data.doc;
    });

    $scope.enableClass = function (school) {
        SchoolsService.classesBySchool(school).then(function (data) {
            $scope.classes = data.data.doc;
            $scope.disabled = false;
        });
    };

    $scope.register = function () {
        //Wachtwoord
        if ($scope.repeatPassword !== $scope.teacher.password) {
            $scope.registerForm.repeatPassword.$setValidity('server', false);
            alert("De wachtwoorden komen niet overeen met elkaar");
        } else {
            $scope.teacher.isTeacher = true;
            AccountsService.accounts.save({}, $scope.teacher);
            alert("Uw account is succesvol geregistreerd");

            // Redirects to homepage
            $location.url("/")
        }

        //test
        console.log("Naam: " + $scope.teacher.firstName);
        console.log("Tussenvoegsel: " + $scope.teacher.preposition);
        console.log("Achternaam: " + $scope.teacher.lastName);
        console.log("Email: " + $scope.teacher.email);
        console.log("School: " + $scope.teacher.school);
        console.log("Klas: " + $scope.teacher.class);
        console.log("Wachtwoord: " + $scope.teacher.password);
    }
}]);