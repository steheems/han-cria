/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 10-1-2015.
 */
ttm.directive('login', ['$rootScope', 'AuthService', 'AccountsService', 'SchoolsService', 'ClassesService', function ($rootScope, AuthService, AccountsService, SchoolsService, ClassesService) {
    'use strict';
    return {
        restrict: 'E',
        templateUrl: 'app/directives/loginDirective.html',
        replace: true,
        link: function ($scope) {
            $scope.currentUser = {};

            $scope.students = [];
            $scope.schools = [];
            $scope.classes = [];

            $scope.errors = {listNames: ''};

            AccountsService.accounts.get({isTeacher: false}).$promise.then(function (data) {
                $scope.students = data.doc;
            });

            SchoolsService.schools.get().$promise.then(function (data) {
                $scope.schools = data.doc;
            });

            ClassesService.classes.get().$promise.then(function (data) {
                $scope.classes = data.doc;
            });

            AuthService.loggedin().then(function (data) {
                $scope.currentUser = data.data;

                if ($scope.currentUser !== '0') {
                    $('#login-button').hide();
                    $('#user-info').show();
                }
            });

            $scope.login = function (user) {
                if (user.id === undefined) {
                    $scope.loginFormS.listNames.$setValidity('server', false);
                    $scope.errors.listNames = 'Er is geen naam geselecteerd';
                } else {
                    $scope.loginFormS.listNames.$setValidity('server', true);
                    $scope.errors.listNames = '';

                    AuthService.login(user).then(function (curUser) {
                        $scope.currentUser = curUser.data;
                        $scope.closeLoginForm();
                        $('#login-button').hide();
                        $('#user-info').show();
                        $scope.user = {};
                        $scope.teacher = {};
                        $rootScope.$broadcast('loggedIn', $scope.currentUser);
                    }, function (err) {
                        console.log(err);
                        $scope.loginFormS.passFieldS.$setValidity('server', false);
                        $scope.errors.passFieldS = 'Het ingevulde wachtwoord is verkeerd';
                    });
                }
            };

            $scope.loginTeacher = function (user) {
                AuthService.loginTeacher(user).then(function (curUser) {
                    $scope.currentUser = curUser.data;
                    $scope.closeLoginForm();
                    $('#login-button').hide();
                    $('#user-info').show();
                    $scope.user = {};
                    $scope.teacher = {};
                    $rootScope.$broadcast('loggedIn', $scope.currentUser);
                }, function (err) {
                    console.log(err);
                    $scope.loginFormT.passFieldT.$setValidity('server', false);
                    $scope.errors.passFieldT = 'De inloggegevens zijn verkeerd';
                });
            };

            $scope.logout = function () {
                AuthService.logout().then(function () {
                    $('#login-button').show();
                    $('#user-info').hide();
                    $rootScope.$broadcast('loggedOut');
                });
            };

            $scope.getFullName = function (account) {
                if (account.firstName !== undefined && account.lastName !== undefined) {
                    var fullName = '';
                    fullName += account.firstName;

                    if (account.preposition !== undefined && account.preposition !== '') {
                        fullName += ' ' + account.preposition;
                    }

                    fullName += ' ' + account.lastName;

                    return fullName;
                }

                return null;
            };

            $scope.openLoginForm = function () {
                var studentForm = $("#tabsContainer"),
                    loginButton = $('#login-button');

                if (studentForm.is(":hidden")) {
                    studentForm.show();
                    $scope.selectTab('Student');
                } else {
                    studentForm.hide();
                }

                $('body').click(function (e) {
                    if (!loginButton.is(e.target) && loginButton.has(e.target).length === 0 &&
                            !studentForm.is(e.target) && studentForm.has(e.target).length === 0) {
                        studentForm.hide();
                    }
                });
            };

            $scope.closeLoginForm = function () {
                $("#tabsContainer").hide();
            };

            $scope.selectTab = function (user) {
                var selectedTab, notSelectedTab, form1, form2;
                form1 = $("#studentLogIn");
                form2 = $("#teacherLogIn");
                if (user === 'Student') {
                    selectedTab = document.getElementById("tab1");
                    selectedTab.setAttribute("style", "background-color:#ccb49a");
                    notSelectedTab = document.getElementById("tab2");
                    notSelectedTab.setAttribute("style", "background-color:white");
                    if (form1.is(":hidden")) {
                        form1.show();
                        form2.hide();
                    }
                } else {
                    selectedTab = document.getElementById("tab2");
                    selectedTab.setAttribute("style", "background-color:#ccb49a");
                    notSelectedTab = document.getElementById("tab1");
                    notSelectedTab.setAttribute("style", "background-color:white");
                    if (form2.is(":hidden")) {
                        form2.show();
                        form1.hide();
                    }
                }
            };
        }
    };
}]);