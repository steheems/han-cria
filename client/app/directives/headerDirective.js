/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 10-1-2015.
 */
ttm.directive('header', function () {
    'use strict';
    return {
        restrict: 'E',
        templateUrl: 'app/directives/headerDirective.html',
        controller: ['$location', function ($location) {
            $('document').ready(function () {
                $('.nav').removeClass('border');

                if ($location.path() === '/') {
                    $('#nav-home').addClass('border');
                } else if ($location.path() === '/customize') {
                    $('#nav-customize').addClass('border');
                }
            });
        }]
    };
});
