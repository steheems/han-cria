/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Ernie on 12-1-2015.
 */
ttm.directive('hover', function () {
    'use strict';
    return {
        restrict: 'A',
        link: function ($scope, element) {
            var emptyDiv = document.createElement('div');

            element.append(emptyDiv);

            element.css('position', 'relative');

            element.hover(function () {
                $(this).children('div').first().addClass('color');
            }, function () {
                $(this).children('div').first().removeClass('color');
            });
        }
    };
});