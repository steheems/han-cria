/*jslint browser:true, nomen: true, plusplus: true */
/*global ttm: true, $: true, alert:true, confirm: true, console: true, d3: true, btoa: true, interact: true */
/**
 * Created by Erwin Heemsbergen on 7-1-2015.
 */
ttm.controller('CustomizeController', ['$rootScope', '$scope', 'AuthService', 'AccountsService', 'PatternsService', 'CarsService', function ($rootScope, $scope, AuthService, AccountsService, PatternsService, CarsService) {
    'use strict';
    $scope.currentView = 'side';

    $('document').ready(function () {
        var xmlSer = new XMLSerializer();
        $scope.defaultDrawAreaSide = xmlSer.serializeToString($('#draw-area-side').children().first()[0]);
        $scope.defaultDrawAreaTop = xmlSer.serializeToString($('#draw-area-top').children().first()[0]);

        AuthService.loggedin().then(function (data) {
            var currentUser = data.data;
            if(currentUser !== '0'){
                $scope.loadUserDesign(currentUser);
                $scope.activateStickers();
                if ($('#stikkers').is(':hidden')) {
                    $('.sticker').css('pointer-events', 'none');
                }
            }
        });
    });
    /*jslint unparam: true*/
    $rootScope.$on('loggedIn', function (event, data) {
        $scope.loadUserDesign(data);
        if ($scope.activeColor !== '' || $scope.activePattern !== '') {
            $scope.initSvg();
        }
        $scope.activateStickers();
        if ($('#stikkers').is(':hidden')) {
            $('.sticker').css('pointer-events', 'none');
        }
    });
    /*jslint unparam: false*/

    $rootScope.$on('loggedOut', function () {
        $scope.loadUserDesign(null);
        if ($scope.activeColor !== '' || $scope.activePattern !== '') {
            $scope.initSvg();
        }
    });

    $scope.stickerCount = {};

    $scope.colors = ['#FF0042', '#402259', '#A93B00', '#00492A'];

    $scope.patterns = [];

    $scope.loadUserDesign = function (user) {
        var xmlSer = new XMLSerializer();

        if (user === null || user === undefined) {
            $('#draw-area-side').html($scope.defaultDrawAreaSide);
            $('#draw-area-top').html($scope.defaultDrawAreaTop);
        } else {
            if (user.designSide !== undefined && user.designSide !== null && user.designSide !== '') {
                $('#draw-area-side').html(user.designSide);
            } else {
                $('#draw-area-side').html($scope.defaultDrawAreaSide);
            }

            if (user.designTop !== undefined && user.designTop !== null && user.designTop !== '') {
                $('#draw-area-top').html(user.designTop);
            } else {
                $('#draw-area-top').html($scope.defaultDrawAreaTop);
            }
        }
    };

    $scope.loadPatterns = function (callback) {
        PatternsService.getPatterns().then(function (res) {
            $scope.patterns = res.data.doc;

            AuthService.loggedin().then(function (res) {
                if (res.data !== '0') {
                    PatternsService.getPersonalPatterns(res.data._id).then(function (res) {
                        $scope.patterns = $scope.patterns.concat(res.data.doc);
                        if (callback) {
                            callback();
                        }
                    });
                }
            });
        });
    };

    $scope.loadPatterns();

    $scope.stickers = ['Vlammen',
        'Vlammen2',
        'sticker auto',
        'sticker auto tijger',
        'stickers ogen',
        'Bliksem',
        'Nummer1',
        'Nummer2',
        'Nummer4',
        'Nummer5',
        'pac',
        'pac2',
        'pac3',
        'pac4',
        'pac5',
        'pac6',
        'pac7',
        'Star1',
        'Star2',
        'Star3',
        'Star4',
        'Star5',
        'Regenboog',
        'rainbow2'];

    $scope.rims = ['velg1',
        'velg2',
        'velg3',
        'velg4',
        'velg5'];

    $scope.activeColor = '';
    $scope.activePattern = '';
    $scope.activeSticker = '';
    $scope.activeRim = '';

    $scope.openTools = function (name) {
        if (name === 'stikkers') {
            $('.sticker').css('pointer-events', 'auto');
        } else {
            $scope.initSvg();
        }

        var menuLinksDiv = $('#menulinks'),
            toolDiv = $('#' + name);

        menuLinksDiv.hide();

        toolDiv.show(0, function () {
            $('#cycle-' + name).cycle({
                fx: 'carousel',
                timeout: 0,
                carouselVertical: true,
                allowWrap: false,
                slides: '> div',
                next: '#scroll-down-' + name,
                prev: '#scroll-up-' + name,
                disabledClass: 'disabled',
                swipe: true
            });
        });
    };

    $scope.menuLinksTerug = function (name) {
        var toolDiv = $('#' + name),
            menuLinksDiv = $('#menulinks');

        toolDiv.hide();

        menuLinksDiv.show();

        $scope.uninitSvg();
        $scope.disableEditing();
    };

    $scope.draaiAuto = function (view) {
        var drawAreaSide = $('#draw-area-side'),
            drawAreaTop = $('#draw-area-top'),
            buttonTopView = $('#button-top-view'),
            buttonSideView = $('#button-side-view');

        if (view === 'side' && drawAreaSide.is(':visible')) {
            drawAreaSide.hide();
            drawAreaTop.show();
            buttonTopView.hide();
            buttonSideView.show();
            $scope.currentView = 'top';
        } else if (view === 'top' && drawAreaTop.is(':visible')) {
            drawAreaTop.hide();
            drawAreaSide.show();
            buttonSideView.hide();
            buttonTopView.show();
            $scope.currentView = 'side';
        }
    };

    $scope.selecteerOptie = function (itemKind, selectedItemContent, $event) {
        var selectedItemElement = $event.target;

        if (itemKind === 'color' && $scope.activeColor !== selectedItemContent) {
            $scope.disableEditing();

            $(selectedItemElement).addClass('selecteerd');

            $scope.enableColoring(selectedItemContent);
        } else if (itemKind === 'pattern' && $scope.activePattern !== selectedItemContent) {
            $scope.disableEditing();

            $(selectedItemElement).addClass('selecteerd');

            $scope.enablePatterning(selectedItemContent);
        } else if (itemKind === 'sticker' && $scope.activeSticker !== selectedItemContent) {
            $scope.disableEditing();

            $(selectedItemElement).addClass('selecteerd');

            $scope.enableStickering(selectedItemContent);
        } else if (itemKind === 'rim' && $scope.activeRim !== selectedItemContent) {
            $scope.disableEditing();

            $(selectedItemElement).addClass('selecteerd');

            $scope.applyRims(selectedItemContent);
        } else if (itemKind === 'rim' && $scope.activeRim === selectedItemContent) {
            $scope.removeRims();
            $scope.disableEditing();
        } else {
            $scope.disableEditing();
        }
    };

    $scope.disableEditing = function () {
        $('.sticker').css('pointer-events', 'none');

        $('.opties').each(function () {
            $(this).removeClass('selecteerd');
        });
        $scope.activeColor = '';
        $scope.activePattern = '';
        $scope.activeSticker = '';
        $scope.activeRim = '';
        $('#auto').css('cursor', '');
    };

    $scope.removeRims = function () {
        var idRimsFront = 'rims-front',
            idRimsBack = 'rims-back';

        $('#' + idRimsFront).remove();
        $('#' + idRimsBack).remove();
        $('#' + idRimsFront + '-background').remove();
        $('#' + idRimsBack + '-background').remove();
    };

    $scope.applyRims = function (rim) {
        var drawArea = $('#draw-area-side').children('svg').first(),
            svg = drawArea[0],
            svgNS = svg.namespaceURI,
            idRimsFront = 'rims-front',
            idRimsBack = 'rims-back',
            patternUrl = 'image/Auto/velgen/' + rim + '.png',
            rimBackgroundBack,
            rimBackgroundFront,
            rimElementBack,
            rimElementFront,
            canvas,
            ctx,
            tempImage,
            canvasDataUrl;

        $('#' + idRimsFront).remove();
        $('#' + idRimsBack).remove();
        $('#' + idRimsFront + '-background').remove();
        $('#' + idRimsBack + '-background').remove();


        rimBackgroundBack = document.createElementNS(svgNS, 'circle');
        rimBackgroundBack.id = idRimsBack + '-background';
        rimBackgroundBack.setAttribute('cx', '158');
        rimBackgroundBack.setAttribute('cy', '395');
        rimBackgroundBack.setAttribute('r', '43');
        rimBackgroundBack.setAttribute('fill', 'white');
        $(rimBackgroundBack).css('pointer-events', 'none');

        rimBackgroundFront = document.createElementNS(svgNS, 'circle');
        rimBackgroundFront.id = idRimsFront + '-background';
        rimBackgroundFront.setAttribute('cx', '687');
        rimBackgroundFront.setAttribute('cy', '395');
        rimBackgroundFront.setAttribute('r', '43');
        rimBackgroundFront.setAttribute('fill', 'white');
        $(rimBackgroundFront).css('pointer-events', 'none');

        rimElementBack = document.createElementNS(svgNS, 'image');
        rimElementBack.id = idRimsBack;
        rimElementBack.setAttribute('x', '114.5');
        rimElementBack.setAttribute('y', '351.5');
        rimElementBack.setAttribute('width', '87');
        rimElementBack.setAttribute('height', '87');

        rimElementFront = document.createElementNS(svgNS, 'image');
        rimElementFront.id = idRimsFront;
        rimElementFront.setAttribute('x', '643.5');
        rimElementFront.setAttribute('y', '351.5');
        rimElementFront.setAttribute('width', '87');
        rimElementFront.setAttribute('height', '87');

        canvas = document.createElement('canvas');
        ctx = canvas.getContext('2d');
        tempImage = new Image();

        tempImage.src = patternUrl;

        tempImage.onload = function () {
            canvas.height = tempImage.height;
            canvas.width = tempImage.width;

            ctx.drawImage(tempImage, 0, 0);

            canvasDataUrl = canvas.toDataURL("image/png");

            rimElementBack.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', canvasDataUrl);
            rimElementFront.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', canvasDataUrl);

            svg.appendChild(rimBackgroundBack);
            svg.appendChild(rimBackgroundFront);

            svg.appendChild(rimElementBack);
            svg.appendChild(rimElementFront);
        };

        $scope.activeRim = rim;
    };

    $scope.enableColoring = function (color) {
        $scope.activeColor = color;
        $('#auto').css('cursor', 'url("image/Auto/Cursor.png") 0 25, auto');
    };

    $scope.enablePatterning = function (pattern) {
        var patternName = pattern.substr(pattern.lastIndexOf('/') + 1),
            svgTop,
            svgSide,
            svgNSTop,
            svgNSSide,
            patternUrl,
            patternElementTop,
            patternImageElementTop,
            patternElementSide,
            patternImageElementSide,
            canvas,
            ctx,
            tempImage,
            canvasDataUrl,
            defsTop,
            defsSide;

        patternName = patternName.substr(0, patternName.lastIndexOf('.'));

        if ($('#' + patternName + '-top').length === 0 && $('#' + patternName + '-side').length === 0) {
            svgTop = $('#draw-area-top').children('svg').first()[0];
            svgSide = $('#draw-area-side').children('svg').first()[0];
            svgNSTop = svgTop.namespaceURI;
            svgNSSide = svgSide.namespaceURI;
            patternUrl = pattern;

            patternElementTop = document.createElementNS(svgNSTop, 'pattern');
            patternElementTop.id = patternName + '-top';
            patternElementTop.setAttribute('class', 'pattern');
            patternElementTop.setAttribute('patternUnits', 'userSpaceOnUse');
            patternElementTop.setAttribute('width', '125');
            patternElementTop.setAttribute('height', '125');

            patternImageElementTop = document.createElementNS(svgNSTop, 'image');
            //patternImageElementTop.setAttributeNS(svgNSTop, 'xlink:href', patternUrl);
            patternImageElementTop.setAttribute('x', '0');
            patternImageElementTop.setAttribute('y', '0');
            patternImageElementTop.setAttribute('width', '125');
            patternImageElementTop.setAttribute('height', '125');

            patternElementSide = document.createElementNS(svgNSSide, 'pattern');
            patternElementSide.id = patternName + '-side';
            patternElementSide.setAttribute('class', 'pattern');
            patternElementSide.setAttribute('patternUnits', 'userSpaceOnUse');
            patternElementSide.setAttribute('width', '125');
            patternElementSide.setAttribute('height', '125');

            patternImageElementSide = document.createElementNS(svgNSSide, 'image');
            //patternImageElementSide.setAttributeNS(svgNSSide, 'xlink:href', patternUrl);
            patternImageElementSide.setAttribute('x', '0');
            patternImageElementSide.setAttribute('y', '0');
            patternImageElementSide.setAttribute('width', '125');
            patternImageElementSide.setAttribute('height', '125');

            canvas = document.createElement('canvas');
            ctx = canvas.getContext('2d');
            tempImage = new Image();

            tempImage.src = patternUrl;

            tempImage.onload = function () {
                canvas.height = tempImage.height;
                canvas.width = tempImage.width;

                ctx.drawImage(tempImage, 0, 0);

                canvasDataUrl = canvas.toDataURL("image/png");

                // Set pattern for Top view

                patternImageElementTop.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', canvasDataUrl);

                patternElementTop.appendChild(patternImageElementTop);

                defsTop = svgTop.querySelector('defs') || svgTop.insertBefore(document.createElementNS(svgNSTop, 'defs'), svgTop.firstChild);

                defsTop.appendChild(patternElementTop);

                // Set pattern for Side view

                patternImageElementSide.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', canvasDataUrl);

                patternElementSide.appendChild(patternImageElementSide);

                defsSide = svgSide.querySelector('defs') || svgSide.insertBefore(document.createElementNS(svgNSSide, 'defs'), svgSide.firstChild);

                defsSide.appendChild(patternElementSide);
            };
        }

        $scope.activePattern = patternName;
        $('#auto').css('cursor', 'url("image/Auto/Cursor.png") 0 25, auto');
    };

    $scope.enableStickering = function (sticker) {
        $('.sticker').css('pointer-events', 'auto');

        if ($scope.stickerCount[$scope.currentView] === undefined) {
            $scope.stickerCount[$scope.currentView] = 0;
        } else {
            $scope.stickerCount[$scope.currentView]++;
        }

        var drawArea = $('#draw-area-' + $scope.currentView).children('svg').first(),
            svg = drawArea[0],
            svgNS = svg.namespaceURI,
            patternImageId = sticker.replace(/\s+/g, '-') + $scope.currentView + $scope.stickerCount[$scope.currentView],
            patternImageUrl = 'image/Auto/stickers/' + sticker + '.png',
            patternImageElement,
            helperRect,
            canvas,
            ctx,
            tempImage;

        patternImageElement = document.createElementNS(svgNS, 'image');
        patternImageElement.setAttribute('x', '0');
        patternImageElement.setAttribute('y', '0');
        patternImageElement.setAttribute('width', '300');
        patternImageElement.setAttribute('height', '200');
        patternImageElement.setAttribute('class', 'draggable sticker');
        patternImageElement.id = patternImageId;

        helperRect = document.createElementNS(svgNS, 'rect');
        helperRect.setAttribute('x', '0');
        helperRect.setAttribute('y', '0');
        helperRect.setAttribute('width', '300');
        helperRect.setAttribute('height', '200');
        helperRect.setAttribute('fill', 'none');
        helperRect.setAttribute('stroke-width', '4');
        helperRect.setAttribute('class', 'sticker-helper');
        helperRect.id = patternImageId + '-helper';

        canvas = document.createElement('canvas');
        ctx = canvas.getContext('2d');
        tempImage = new Image();

        tempImage.src = patternImageUrl;

        tempImage.onload = function () {
            canvas.height = tempImage.height;
            canvas.width = tempImage.width;

            ctx.drawImage(tempImage, 0, 0);

            var canvasDataUrl = canvas.toDataURL("image/png");

            // Set image url

            patternImageElement.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', canvasDataUrl);

            // Add image to SVG

            svg.appendChild(helperRect);
            svg.appendChild(patternImageElement);

            $(patternImageElement).hover(function () {
                $(helperRect).attr('stroke', 'red');
            }, function () {
                $(helperRect).attr('stroke', 'none');
            });
        };
    };

    $scope.activateStickers = function () {
        var stickersSide = $(document).find('.sticker'),
            stickersTop = $(document).find('.sticker');

        /*jslint unparam: true*/
        stickersSide.each(function (index, sticker) {
            if ($scope.stickerCount.side === undefined) {
                $scope.stickerCount.side = 0;
            } else {
                $scope.stickerCount.side++;
            }

            var helperRectId = $(sticker).attr('id') + '-helper';

            $(sticker).hover(function () {
                $('#' + helperRectId).attr('stroke', 'red');
            }, function () {
                $('#' + helperRectId).attr('stroke', 'none');
            });
        });

        stickersTop.each(function (index, sticker) {
            if ($scope.stickerCount.top === undefined) {
                $scope.stickerCount.top = 0;
            } else {
                $scope.stickerCount.top++;
            }

            var helperRectId = $(sticker).attr('id') + '-helper';

            $(sticker).hover(function () {
                $('#' + helperRectId).attr('stroke', 'red');
            }, function () {
                $('#' + helperRectId).attr('stroke', 'none');
            });
        });
        /*jslint unparam: false*/
    };

    $scope.deleteSticker = function (sticker) {
        var stickerId = sticker.id;
        var helperId = stickerId + '-helper';

        $('#' + helperId).remove();
        $('#' + stickerId).remove();

        $('#delete-zone').css('background-color', 'rgba(0, 0, 0, 0.7)');

        $scope.disableEditing();
    };

    $scope.resetDrawing = function () {
        if (confirm('Weet je zeker dat je helemaal opnieuw wil beginnen?')) {
            $('#draw-area-top').html($scope.defaultDrawAreaTop);
            $('#draw-area-side').html($scope.defaultDrawAreaSide);

            $scope.initSvg();
        }
    };

    $scope.initSvg = function () {
        $('.draw-area').children('svg').each(function () {
            $(this).children('g').first().children().each(function () {
                $(this).click(function () {
                    if ($scope.activeColor !== '') {
                        this.setAttribute('fill', $scope.activeColor);
                    } else if ($scope.activePattern !== '') {
                        this.setAttribute('fill', 'url(#' + $scope.activePattern + '-' + $scope.currentView + ')');
                    }
                });

                $(this).hover(function () {
                    $(this).attr('stroke', 'red');
                }, function () {
                    $(this).attr('stroke', 'black');
                });
            });
        });
    };

    $scope.uninitSvg = function () {
        $('.draw-area').children('svg').each(function () {
            $(this).children('g').first().children().each(function () {
                $(this).unbind('click mouseenter mouseleave');
            });
        });
    };

    $scope.showDropzone = function ($event) {
        var dropzoneElem = $('#dropzone-div');
        if (dropzoneElem.is(':hidden')) {
            AuthService.loggedin().then(function (data) {
                var currentUser = data.data;

                if (currentUser === '0') {
                    throw new Error('Need to be logged in to upload something');
                }

                dropzoneElem.show();
                dropzoneElem.addClass('dropzone');
                dropzoneElem.dropzone({
                    url: "/patterns/" + currentUser._id,
                    acceptedFiles: 'image/*',
                    init: function () {
                        this.on('success', function () {
                            $scope.loadPatterns(function () {
                                console.log($scope.patterns);
                            });
                        });
                    }
                });
                $($event.target).addClass('selecteerd');
            });

        } else {
            dropzoneElem.hide();
            $($event.target).removeClass('selecteerd');
        }
    };

    $scope.showColorPicker = function ($event) {
        if ($scope.activeColor !== '') {
            $('.opties').each(function () {
                $(this).removeClass('selecteerd');
            });
        }

        var colorPickerElem = $('#wColorPicker');
        if (colorPickerElem.is(':hidden')) {
            colorPickerElem.show();
            $($event.target).addClass('selecteerd');
        } else {
            colorPickerElem.hide();
            $scope.disableEditing();
            $($event.target).removeClass('selecteerd');
        }
    };

    $scope.hideColorPicker = function () {
        var colorPickerElem = $('#wColorPicker');
        colorPickerElem.hide();
    };

    $("#wColorPicker").wColorPicker({
        color: '#FF00FF',
        onSelect: function (color) {
            $("#color-picker-button").css('background', color).val(color);
            $scope.enableColoring(color);
            $scope.hideColorPicker();
        },
        onMouseover: function (color) {
            $("#color-picker-button").css('background', color).val(color);
        },
        onMouseout: function (color) {
            $("#color-picker-button").css('background', color).val(color);
        }
    });

    $scope.slideMenusIn = function () {
        $('document').ready(function () {
            $('body').css('overflow', 'hidden');

            var leftMenu = $('#menulinks'),
                rightMenu = $('#menurechts'),
                header = $('header'),
                leftMenuWidth,
                rightMenuWidth;

            rightMenu.css('top', header.outerHeight() + 'px');

            $(window).resize(function () {
                rightMenu.css('top', header.outerHeight() + 'px');
            });

            leftMenuWidth = leftMenu.width();
            leftMenu.css('left', '-' + leftMenuWidth + 'px');

            rightMenuWidth = rightMenu.width();
            rightMenu.css('right', '-' + rightMenuWidth + 'px');

            leftMenu.animate({
                left: 0
            }, 1000, 'linear');
            rightMenu.animate({
                right: 0
            }, 1000, 'linear');

            /*$('header').animate({
             margin: '0 10%',
             padding: '1% 0'
             }, 1010, 'linear');*/
        });
    };

    $scope.saveSvgToImage = function () {
        AuthService.loggedin().then(function (data) {
            var currentUser = data.data,
                htmlSide = d3.select('#draw-area-side').node().innerHTML,
                imgsrcSide = 'data:image/svg+xml;base64,' + btoa(htmlSide),
                canvasSide = document.createElement("canvas"),
                contextSide = canvasSide.getContext("2d"),
                imageSide = new Image(),
                htmlTop = d3.select('#draw-area-top').node().innerHTML,
                imgsrcTop = 'data:image/svg+xml;base64,' + btoa(htmlTop),
                canvasTop = document.createElement("canvas"),
                contextTop = canvasTop.getContext("2d"),
                imageTop = new Image(),
                height = 300,
                width = 720;

            if (currentUser === '0') {
                alert('Je moet eerst inloggen voordat je kan opslaan');
                throw new Error('Need to be logged in to save something');
            }

            if (confirm('Weet je zeker dat je wilt opslaan? Dit overschrijft je huidige auto.')) {

                imageSide.src = imgsrcSide;
                imageSide.width = canvasSide.width = width;
                imageSide.height = canvasSide.height = height - 15;

                imageTop.src = imgsrcTop;
                imageTop.width = canvasTop.width = width - 640;
                imageTop.height = canvasTop.height = height - 256;

                imageSide.onload = function () {
                    var sourceX = 38,
                        sourceY = 35,
                        sourceWidth = width - 495,
                        sourceHeight = height - 170,
                        destWidth = width,
                        destHeight = height + 100,
                        destX = 0,
                        destY = 0;

                    contextSide.drawImage(imageSide, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);

                    var canvasdata = canvasSide.toDataURL("image/png");

                    canvasdata = canvasdata.replace(/^data:image\/png;base64,/, "");

                    CarsService.uploadDesign(currentUser._id, canvasdata, 'side');
                };

                imageTop.onload = function () {
                    var sourceX = 28,
                        sourceY = 20,
                        sourceWidth = width + 1450,
                        sourceHeight = height + 550,
                        destWidth = width,
                        destHeight = height,
                        destX = 0,
                        destY = 0;

                    contextTop.drawImage(imageTop, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
                    //contextTop.drawImage(imageTop, 30, 10, 240, 150, 0, 0, width, height);

                    var canvasdata = canvasTop.toDataURL("image/png");

                    canvasdata = canvasdata.replace(/^data:image\/png;base64,/, "");

                    CarsService.uploadDesign(currentUser._id, canvasdata, 'top');
                };

                AccountsService.updateDesign(currentUser._id, $('#draw-area-side').html(), $('#draw-area-top').html());

                alert('Auto opgeslagen');
            }
        });
    };

    /**
     * Use interactJS to make things draggable
     */
    var deleteZoneActiveY = 420;

    interact('.draggable').draggable({
        // enable inertial throwing
        //inertia: true,

        // keep the element within the area of it's parent
        restrict: {
            restriction: 'parent',
            endOnly: false,
            elementRect: {top: 0.5, left: 0.5, bottom: 0.5, right: 0.5}
        },

        // call this function on every dragmove event
        onmove: function (event) {
            $('#delete-zone').show();

            var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('y')) || 0) + event.dy,
                id = target.id + '-helper',
                helper = $('#' + id);

            if ($(target).is('.sticker')) {
                helper.attr('y', y);
                helper.attr('x', x);
            }

            target.setAttribute('x', x);
            target.setAttribute('y', y);

            if (y > deleteZoneActiveY) {
                $('#delete-zone').css('background-color', 'rgba(255, 0, 0, 0.7)');
                $('#delete-zone-img').attr('src','image/Iconen/Verwijderen-actief.png');

            } else {
                $('#delete-zone').css('background-color', 'rgba(0, 0, 0, 0.7)');
                $('#delete-zone-img').attr('src','image/Iconen/Verwijderen.png');
            }
        },

        // call this function on stop moving
        onend: function (event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('x')) || 0),
                y = (parseFloat(target.getAttribute('y')) || 0);

            if (y > deleteZoneActiveY) {
                $scope.deleteSticker(target);
            }

            $('#delete-zone').hide();
        }
    }).resizable(true).on('resizemove', function (event) {
        var target = event.target,
            newWidth = parseFloat(target.getAttribute('width')) + event.dx,
            newHeight = parseFloat(target.getAttribute('height')) + event.dy,
            id = target.id + '-helper';

        if ($(target).is('.sticker')) {
            if (newHeight > 10) {
                $('#' + id).attr('height', newHeight);
            }

            if (newWidth > 10) {
                $('#' + id).attr('width', newWidth);
            }
        }

        // update the element's style
        if (newHeight > 10) {
            target.setAttribute('height', newHeight);
        }

        if (newWidth > 10) {
            target.setAttribute('width', newWidth);
        }
    });
}]);