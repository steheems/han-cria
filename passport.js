/*jslint nomen: true */
/*global require: true, module: true */
var passport = require('passport'),
    Account = require('mongoose').model('Account'),
    path = require('path');

module.exports = function () {
    'use strict';
    // Serialize sessions
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // Deserialize sessions
    passport.deserializeUser(function (id, done) {
        Account.findOne({
            _id: id
        }, '-salt -password', function (err, user) {
            done(err, user);
        });
    });

    // Initialize strategies
    require('./config/localStrategy')();
};