/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'Account',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName);

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE ' + modelName);

    var doc = new Model(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        if (err !== null) {
            retObj.errMessage = err.message;
        }

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    if (req.params.id) {
        Model.findById(req.params.id).exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Model.find(req.query).exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.update = function (req, res) {
    'use strict';
    console.log('Updating ' + modelName + '\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {},
        options = {multi: false},
        callback = function (err, doc) {
            if (err === null) {
                for (var key in req.body) {
                    if (req.body.hasOwnProperty(key)) {
                        if (key === 'password' && doc.password !== req.body.password) {
                            doc.password = req.body.password;
                            doc.encrypted = false;
                        } else if (key === 'password' && doc.password === req.body.password) {
                            doc.encrypted = true;
                        } else {
                            doc[key] = req.body[key];
                        }
                    }
                }

                doc.save();
            }

            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    //Model.findOneAndUpdate(conditions, update, options, callback);
    Model.findOne(conditions, callback);
};

exports.updateLevelUnlocked = function (req, res) {
    'use strict';
    console.log('Updating LEVEL UNLOCKED for ' + modelName + '\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {
            levelUnlocked: req.params.lvl || 0
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Model.findOneAndUpdate(conditions, update, options, callback);
};

exports.updateDesign = function (req, res) {
    'use strict';
    console.log('Updating DESIGN for ' + modelName + '\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {},
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    
    if(req.body.designSide !== undefined && req.body.designSide !== null) {
        update.designSide = req.body.designSide;
    }

    if(req.body.designTop !== undefined && req.body.designTop !== null) {
        update.designTop = req.body.designTop;
    }
    

    Model.findOneAndUpdate(conditions, update, options, callback);
};

exports.delete = function (req, res) {
    'use strict';
    console.log('Deleting ' + modelName + '\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Model.remove(conditions, callback);
};