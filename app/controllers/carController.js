/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var fs = require('fs');

exports.add = function (req, res) {
    'use strict';
    if(req.params.view.toLowerCase() === 'side' || req.params.view.toLowerCase() === 'top') {
        var img_data = new Buffer(req.body.data, 'base64'),
            target_model_dir = myDirName + '/uploads/cars',
            target_dir = target_model_dir + '/' + req.params.id,
            target_path = target_dir + '/' + req.params.view + '.png';

        // First create the model directory e.g.: Pattern
        fs.mkdir(target_model_dir, function (err) {
            if (err && err.code !== 'EEXIST') {
                throw err;
            }

            // Second, create the user's directory using the user's id
            fs.mkdir(target_dir, function (err) {
                if (err && err.code !== 'EEXIST') {
                    throw err;
                }
                // Third, create the file using the image data received
                fs.writeFile(target_path, img_data, function (err) {
                    if (err && err.code !== 'EEXIST') {
                        throw err;
                    }
                    res.send('Car ' + req.params.view + ' image uploaded to: ' + target_path);
                });
            });
        });
    } else {
        res.sendStatus(400);
    }
};