/**
 * Created by erwin on 03-02-15.
 */
/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'Highscore',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName);

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE ' + modelName);

    req.body.level = req.params.level;

    var doc = new Model(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        if (err !== null) {
            retObj.errMessage = err.message;
        }

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    var ids = [];

    Model.aggregate({$match: {'level': parseInt(req.params.level)}}, {$sort: {'score': 1}}, {$group : {_id:'$account', id: {$first: '$_id'}}}, {$project: {id:'$id', _id:0}}).exec(function (err, docs) {
        if (err) {
            throw new Error(err);
        }

        for (var i = 0; i < docs.length; i++) {
            ids.push(docs[i].id);
        }

        Model.find().in('_id', ids).sort('score').limit(25).exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    });


};

exports.retrieveBySchool = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName + ' BY SCHOOL');

    if (req.params.id) {
        var ids = [];

        Model.aggregate({$match: {'level': parseInt(req.params.level)}}, {$sort: {'score': 1}}, {$group : {_id:'$account', id: {$first: '$_id'}}}, {$project: {id:'$id', _id:0}}).exec(function (err, docs) {
            for (var i = 0; i < docs.length; i++) {
                ids.push(docs[i].id);
            }
            Model.find({
                school: req.params.id
            }).in('_id', ids).sort('score').limit(25).exec(function (err, doc) {
                if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                    err = 'Could not find item with this query';
                }
                var retObj = {
                    meta: {"action": "retrieve by school", "timestamp": new Date(), filename: __filename},
                    doc: doc,
                    err: err
                };

                return res.send(retObj);
            });
        });
    } else {
        return res.sendStatus(406);
    }
};

exports.retrieveByClass = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName + ' BY CLASS');

    if (req.params.id) {
        var ids = [];

        Model.aggregate({$match: {'level': parseInt(req.params.level)}}, {$sort: {'score': 1}}, {$group : {_id:'$account', id: {$first: '$_id'}}}, {$project: {id:'$id', _id:0}}).exec(function (err, docs) {
            for (var i = 0; i < docs.length; i++) {
                ids.push(docs[i].id);
            }

            Model.find({
                class: req.params.id
            }).in('_id', ids).sort('score').limit(25).exec(function (err, doc) {
                if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                    err = 'Could not find item with this query';
                }
                var retObj = {
                    meta: {"action": "retrieve by class", "timestamp": new Date(), filename: __filename},
                    doc: doc,
                    err: err
                };

                return res.send(retObj);
            });
        });
    } else {
        return res.sendStatus(406);
    }
};

exports.retrieveByAccount = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName + ' BY ACCOUNT');

    if (req.params.id) {
        Model.find({level:req.params.level, account: req.params.id}).sort('score').limit(25).exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve by account", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        return res.sendStatus(406);
    }
};