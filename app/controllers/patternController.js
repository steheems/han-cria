/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'Pattern',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName),
    fs = require('fs'),
    async = require('async'),
    readdir = require('readdir');

var getOnlyFiles = function (filePath, callback) {
    'use strict';
    fs.stat(filePath, function (err, stats) {
        if (stats.isFile()) {
            callback(err, filePath);
        } else {
            callback(err, null);
        }
    });
};

exports.retrieveAll = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    var readDir = myDirName + '/uploads/' + modelName;
    readdir.read(readDir, ['*.png', '*.jpg', '*.gif', '*.jpeg', '*.bmp'], function (err, files) {
        if (err) {
            throw err;
        }

        var filesPath = files.map(function (file) {
            return readDir + '/' + file;
        }), retObj = {
            meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
            doc: filesPath,
            err: err
        };

        return res.send(retObj);
    });
};

exports.retrievePersonal = function (req, res) {
    'use strict';
    console.log('RETRIEVE personal ' + modelName);

    var readDir = myDirName + '/uploads/' + modelName + '/' + req.params.id;
    readdir.read(readDir, ['*.png', '*.jpg', '*.gif', '*.jpeg', '*.bmp'], function (err, files) {
        if (err) {
            throw err;
        }

        var filesPath = files.map(function (file) {
            return readDir + '/' + file;
        }), retObj = {
            meta: {"action": "retrieve personal", "timestamp": new Date(), filename: __filename},
            doc: filesPath,
            err: err
        };
        return res.send(retObj);
    });
};

exports.add = function (req, res) {
    'use strict';
    var tmp_path = req.files.file.path,
        target_model_dir = myDirName + '/uploads/' + modelName,
        target_dir = target_model_dir + '/' + req.params.id,
        target_path =  target_dir + '/' + req.files.file.name;

    // First create the model directory e.g.: Pattern
    fs.mkdir(target_model_dir, function (err) {
        if (err && err.code !== 'EEXIST') {
            throw err;
        }

        // Second, create the user's directory using the user's id
        fs.mkdir(target_dir, function (err) {
            if (err && err.code !== 'EEXIST') {
                throw err;
            }
            // Third, move the file from the temporary location to the intended location
            fs.rename(tmp_path, target_path, function (err) {
                if (err) {
                    throw err;
                }
                // Fourth, delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                fs.unlink(tmp_path, function () {
                    if (err) {
                        throw err;
                    }
                    res.send('File uploaded to: ' + target_path + ' - ' + req.files.file.size + ' bytes');
                });
            });
        });
    });
};