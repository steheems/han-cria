/**
 * Created by Ernie on 27-1-2015.
 */
/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'Upgrade',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName);

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    Model.find({account: req.params.id}).exec(function (err, doc) {
        if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
            err = 'Could not find item with this query';
        }
        var retObj = {
            meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        return res.send(retObj);
    });
};

exports.createOrUpdate = function (req, res) {
    'use strict';

    Model.findOne({account: req.params.id}, function (err, doc) {
        if (doc === null) {
            console.log('CREATE ' + modelName);

            var newDoc = new Model(req.body);

            newDoc.account = req.params.id;

            newDoc.save(function (err) {
                var retObj = {
                    meta: {"action": "create", "timestamp": new Date(), filename: __filename},
                    doc: newDoc,
                    err: err
                };

                if (err !== null) {
                    retObj.errMessage = err.message;
                }

                return res.send(retObj);
            });
        } else {
            console.log('UPDATE ' + modelName + ' with ACCOUNT ' + req.params.id);

            var callback = function (err, doc) {
                var retObj = {
                    meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };

            doc.fuel = req.body.fuel || doc.fuel;
            doc.parts = req.body.parts || doc.parts;
            doc.body = req.body.body || doc.body;
            doc.steer = req.body.steer || doc.steer;
            doc.brakes = req.body.brakes || doc.brakes;
            doc.tireWidth = req.body.tireWidth || doc.tireWidth;
            doc.tireSize = req.body.tireSize || doc.tireSize;
            doc.tirePressure = req.body.tirePressure || doc.tirePressure;
            doc.suspension = req.body.suspension || doc.suspension;
            doc.gear = req.body.gear || doc.gear;
            doc.drive = req.body.drive || doc.drive;
            doc.airIntake = req.body.airIntake || doc.airIntake;
            doc.engine = req.body.engine || doc.engine;
            doc.health = req.body.health || doc.health;
            doc.driftRecover = req.body.driftRecover || doc.driftRecover;
            doc.steerSpeed = req.body.steerSpeed || doc.steerSpeed;
            doc.deceleration = req.body.deceleration || doc.deceleration;
            doc.acceleration = req.body.acceleration || doc.acceleration;
            doc.topSpeed = req.body.topSpeed || doc.topSpeed;

            doc.save(callback);
        }
    });
};