/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'Color',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName);

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    if (req.params.id) {
        Model.findById(req.params.id).exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Model.find().exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};