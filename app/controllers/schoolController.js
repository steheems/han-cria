/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, __filename: true */
var modelName = 'School',
    mongoose = require('mongoose'),
    Model = mongoose.model(modelName);

exports.create = function (req, res) {
    'use strict';
    console.log('CREATE ' + modelName);

    var doc = new Model(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date(), filename: __filename},
            doc: doc,
            err: err
        };

        if (err !== null) {
            retObj.errMessage = err.message;
        }

        return res.send(retObj);
    });
};

exports.retrieve = function (req, res) {
    'use strict';
    console.log('RETRIEVE ' + modelName);

    if (req.params.id) {
        Model.findById(req.params.id).exec(function (err, doc) {
            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                err = 'Could not find item with this query';
            }
            var retObj = {
                meta: {"action": "retrieve one", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
    } else {
        Model.find().exec(function (err, docs) {
            var retObj = {
                meta: {"action": "retrieve all", "timestamp": new Date(), filename: __filename},
                doc: docs,
                err: err
            };

            return res.send(retObj);
        });
    }
};

exports.update = function (req, res) {
    'use strict';
    console.log('Updating request\n', req.params.id);

    var conditions = {_id: req.params.id},
        update = {
            name: req.body.name || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Model.findOneAndUpdate(conditions, update, options, callback);
};

exports.delete = function (req, res) {
    'use strict';
    console.log('Deleting ' + modelName + '\n', req.params.id);

    var conditions = {_id: req.params.id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", "timestamp": new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Model.remove(conditions, callback);
};