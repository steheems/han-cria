/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true, Buffer: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');


var modelName = 'Account',
    collectionName = 'accounts';

var schemaName = new Schema({
    firstName: {type: String, required: true},
    preposition: {type: String},
    lastName: {type: String, required: true},
    isTeacher: {type: Boolean, required: true, default: false},
    email: {type: String},
    levelUnlocked: {type: Number, default: 0},
    designSide: {type: String, default: ''},
    designTop: {type: String, default: ''},
    class: {type: Schema.ObjectId, ref: 'Class'},
    password: {type: String, default: ''},
    salt: {type: String},
    encrypted: {type: Boolean, default: false}
});

schemaName.path('email').validate(function (val) {
    'use strict';
    var emailRegex = /^[A-Z0-9_%+\-]+@[A-Z0-9\-]+\.[A-Z]{2,6}/ig;
    return (val !== undefined && val !== null && emailRegex.test(val));
}, 'Invalid email address');

schemaName.path('password').validate(function (val) {
    'use strict';
    return (val.length >= 6);
}, 'Password is too short');

/**
 * Hook a pre save method to hash the password
 */
schemaName.pre('save', function (next) {
    'use strict';
    if (this.password && this.password.length >= 6 && !this.encrypted) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
        this.encrypted = true;
    } else if (this.password.length < 6) {
        throw new Error('Password is too short');
    }

    next();
});

/**
 * Hook a pre save method to check if an email address has been entered
 * when the account belongs to a teacher
 */
schemaName.pre('save', function (next) {
    'use strict';
    if (this.isTeacher && this.email === undefined) {
        var err = new Error('teacher accounts require an email address');
        next(err);
    } else {
        next();
    }
});

/**
 * Hook a pre save method for checking unique email address
 * (built-in unique won't work because email addresses can be null)
 */
schemaName.pre('save', function (next) {
    'use strict';
    if (this.isTeacher && !this._id) {
        this.emailInUse(function (inUse) {
            if (inUse) {
                var err = new Error('email address already in-use');
                next(err);
            } else {
                next();
            }
        });
    } else {
        next();
    }
});

/**
 * Hook a pre save method for checking if a student is in a class and school
 */
schemaName.pre('save', function (next) {
    'use strict';
    var err;

    if (!this.isTeacher) {
        if (!this.class) {
            err = new Error('account has no class associated with it');
            next(err);
        } else {
            next();
        }
    } else {
        next();
    }
});

/**
 * Method for finding out if an email adress is already in use
 */
schemaName.methods.emailInUse = function (callback) {
    'use strict';

    this.model(modelName).findOne({
        email: this.email
    }, function (err, user) {
        if (!err) {
            if (!user) {
                callback(false);
            } else {
                callback(true);
            }
        } else {
            callback(null);
        }
    });
};

/**
 * Create instance method for hashing a password
 */
schemaName.methods.hashPassword = function (password) {
    'use strict';
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
    }

    return password;
};

/**
 * Create instance method for authenticating user
 */
schemaName.methods.authenticate = function (password) {
    'use strict';
    return this.password === this.hashPassword(password);
};


mongoose.model(modelName, schemaName, collectionName);