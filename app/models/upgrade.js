/**
 * Created by Ernie on 27-1-2015.
 */
/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var modelName = 'Upgrade',
    collectionName = 'upgrades';

var schemaName = new Schema({
    account: {type: Schema.ObjectId, ref: 'Account', required: true, unique: true},
    fuel: {type: Number, default: 1, required: true},
    parts: {type: Number, default: 1, required: true},
    body: {type: Number, default: 0, required: true},
    steer: {type: Number, default: 0, required: true},
    brakes: {type: Number, default: 0, required: true},
    tireWidth: {type: Number, default: 0, required: true},
    tireSize: {type: Number, default: 0, required: true},
    tirePressure: {type: Number, default: 1, required: true},
    suspension: {type: Number, default: 0, required: true},
    gear: {type: Number, default: 0, required: true},
    drive: {type: Number, default: 1, required: true},
    airIntake: {type: Number, default: 0, required: true},
    engine: {type: Number, default: 1, required: true},
    health: {type: Number, default: 5, required: true},
    driftRecover: {type: Number, default: 295, required: true},
    steerSpeed: {type: Number, default: 290, required: true},
    deceleration: {type: Number, default: 430, required: true},
    acceleration: {type: Number, default: 265, required: true},
    topSpeed: {type: Number, default: 625, required: true}
});

var checkZeroOrOne = function (val) {
    'use strict';
    return (val === 0 || val === 1);
};

schemaName.path('fuel').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('parts').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('body').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('steer').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('brakes').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('tireWidth').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('tireSize').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('tirePressure').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('suspension').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('gear').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('drive').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('airIntake').validate(checkZeroOrOne, 'Input can only be 0 or 1');
schemaName.path('engine').validate(checkZeroOrOne, 'Input can only be 0 or 1');

mongoose.model(modelName, schemaName, collectionName);