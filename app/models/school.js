/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var modelName = 'School',
    collectionName = 'schools';

var schemaName = new Schema({
    name: {type: String, required: true, unique: true}
});

mongoose.model(modelName, schemaName, collectionName);