/**
 * Created by erwin on 03-02-15.
 */
/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    School = mongoose.model('School'),
    Class = mongoose.model('Class'),
    Account = mongoose.model('Account');


var modelName = 'Highscore',
    collectionName = 'highscores';

var schemaName = new Schema({
    score: {type: String, required: true},
    level: {type: Number, required: true},
    account: {type: Schema.ObjectId, ref: 'Account', required: true},
    class: {type: Schema.ObjectId, ref: 'Class'},
    school: {type: Schema.ObjectId, ref: 'School'}
});

schemaName.pre('save', function (next) {
    'use strict';
    if(this !== null) {
        var _this = this;

        if (_this.class === undefined || _this.class === '') {
            Account.findById(_this.account, function (err, doc) {
                if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                    err = 'Could not find ACCOUNT with this query';
                }

                if (err) {
                    next(err);
                } else {
                    console.log(doc.class);
                    _this.class = doc.class;

                    if (_this.school === undefined || _this.school === '') {
                        console.log(_this);
                        Class.findById(_this.class, function (err, doc) {
                            if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                                err = 'Could not find CLASS with this query';
                            }

                            if (err) {
                                next(err);
                            } else {
                                _this.school = doc.school;
                                next();
                            }
                        });
                    } else {
                        next();
                    }
                }
            });
        } else if (_this.school === undefined || _this.school === '') {
            Class.findById(_this.class, function (err, doc) {
                if ((doc === undefined || doc === null) && (err === undefined || err === null)) {
                    err = 'Could not find CLASS with this query';
                }

                if (err) {
                    next(err);
                } else {
                    _this.school = doc.school;
                    next();
                }
            });
        }
    } else {
        next(new Error('Input is NULL'));
    }

    /*if (this.isTeacher && this.email === undefined) {
        var err = new Error('teacher accounts require an email address');
        next(err);
    } else {
        next();
    }*/
});

mongoose.model(modelName, schemaName, collectionName);