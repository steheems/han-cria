/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var modelName = 'Color',
    collectionName = 'colors';

var schemaName = new Schema({
    code: {type: String, required: true, unique: true},
    gold: {type: Number, default: 0}
});

mongoose.model(modelName, schemaName, collectionName);