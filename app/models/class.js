/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var modelName = 'Class',
    collectionName = 'classes';

var schemaName = new Schema({
    name: {type: String, required: true},
    school: {type: Schema.ObjectId, ref: 'School', required: true}
});

mongoose.model(modelName, schemaName, collectionName);