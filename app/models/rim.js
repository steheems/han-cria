/*jslint plusplus: true, nomen: true */
/*global exports: true, require: true, console: true */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var modelName = 'Rim',
    collectionName = 'rims';

var schemaName = new Schema({
    url: {type: String, required: true, unique: true},
    gold: {type: Number, default: 0}
});

mongoose.model(modelName, schemaName, collectionName);